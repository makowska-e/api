﻿export class CreateUserDTO {
    readonly id: string;
    readonly name: string;
    readonly surname: string;
    readonly company: string;
  }