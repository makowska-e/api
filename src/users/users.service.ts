import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly postModel: Model<User>) { }

  async addUser(createUserDTO: CreateUserDTO): Promise<User> {
    try {
      const newUser = await this.postModel(createUserDTO);
      return newUser.save();      
    }
    catch (err) {
      console.log("error", err)
    }
  }  

  async getUsers(): Promise<User[]> {
    const posts = await this.postModel.find().exec();
    return posts;
  }

  async getUser(userID): Promise<User> {
    const user = await this.postModel
      .findById(userID)
      .exec();
    return user;
  }  

  async editUser(userID, createUserDTO: CreateUserDTO): Promise<User> {
    const editedUser = await this.postModel
      .findByIdAndUpdate(userID, createUserDTO, { new: true });
    return editedUser;
  }
  
  async deleteUser(userID): Promise<any> {
    const deletedUser = await this.postModel
      .findByIdAndRemove(userID);
    return deletedUser;
  }
} 
