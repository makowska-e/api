import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Put, Query, Delete } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { ValidateObjectId } from './shared/pipes/validate-object-id.pipes';

@Controller('users')
export class UsersController {

  constructor(private usersService: UsersService) { }

  @Post('/add')
  async addUser(@Res() res, @Body() createUserDTO: CreateUserDTO) {
    const newUser = await this.usersService.addUser(createUserDTO);
    return res.status(HttpStatus.OK).json({
      message: 'User has been submitted successfully!',
      post: newUser,
    });
  }

  @Get('/')
  async getUsers(@Res() res) {
    const users = await this.usersService.getUsers();
    return res.status(HttpStatus.OK).json(users);
  }

  @Get('/:userID')
  async getPost(@Res() res, @Param('userID', new ValidateObjectId()) userID) {
    console.log("user", userID)
    const post = await this.usersService.getUser(userID);
    if (!post) {
      throw new NotFoundException('User does not exist!');
    }
    return res.status(HttpStatus.OK).json(post);
  }

  @Put('/edit')
  async editPost(
    @Res() res,
    @Query('userID', new ValidateObjectId()) userID,
    @Body() createUserDTO: CreateUserDTO,
  ) {
    const editedUser = await this.usersService.editUser(userID, createUserDTO);
    if (!editedUser) {
        throw new NotFoundException('User does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Post has been successfully updated',
      post: editedUser,
    });
  }

  @Delete('/delete')
  async deleteUser(@Res() res, @Query('userID', new ValidateObjectId()) userID) {
    const deletedUser = await this.usersService.deleteUser(userID);
    if (!deletedUser) {
        throw new NotFoundException('User does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'User has been deleted!',
      post: deletedUser,
    });
  }

}